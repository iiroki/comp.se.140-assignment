const fs = require('fs')
const receive = require('../mq-util/receive')

const MQ_HOST = process.env.MQ_HOST
const OUTPUT_FILE = process.env.OUTPUT_FILE
const TOPICS = ['#']
const STARTUP_DELAY_MS = 4000

/**
 * 1. Remove the output file if it already exists
 * 2. Create a write stream to the new output file
 * 3. Wait for 5 secs before receiving messages
 * 4. Append altered received messages to the output file
 */
const main = async () => {
  // Remove the output file if it exists
  try {
    fs.unlinkSync(OUTPUT_FILE)
  } catch {}

  const file = fs.createWriteStream(OUTPUT_FILE, { flags: 'a' })
  await new Promise(resolve => setTimeout(resolve, STARTUP_DELAY_MS))

  receive(MQ_HOST, TOPICS, msg => {
    // Append to the output file 
    file.write(`${new Date().toISOString()} Topic ${msg.fields.routingKey}: ${msg.content.toString()}\n`)
  })
}

main()
