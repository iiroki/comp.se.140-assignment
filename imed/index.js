const receive = require('../mq-util/receive')
const send = require('../mq-util/send')

const MQ_HOST = process.env.MQ_HOST
const RECEIVE_TOPICS = ['my.o']
const SEND_TOPIC = 'my.i'
const WAIT_BEFORE_SEND_MS = 1000
const STARTUP_DELAY_MS = 4000

/**
 * 1. Wait for 5 secs before receiving messages
 * 2. Wait for messages from wanted topics
 * 3. Wait for 1 sec after receiving a message
 * 4. Send an altered message to another topic
 */
const main = async () => {
  await new Promise(resolve => setTimeout(resolve, STARTUP_DELAY_MS))

  receive(MQ_HOST, RECEIVE_TOPICS, async msg => {
    await new Promise(resolve => setTimeout(resolve, WAIT_BEFORE_SEND_MS))
    send(MQ_HOST, SEND_TOPIC, `Got ${msg.content.toString()}`)
  })
}

main()
