const http = require('http')
const fs = require('fs')

const PORT = process.env.PORT
const OUTPUT_FILE = process.env.OUTPUT_FILE

// Create an HTTP server that returns content of the output file
http.createServer((_req, res) => {
  let data = ''
  try {
    data = fs.readFileSync(OUTPUT_FILE, 'utf8').toString()
  } catch {}
  res.writeHead(200)
  res.end(data)
}).listen(PORT)
