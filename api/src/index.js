const api = require('./api')

// Defaults to port 8000 if environment variable is not set
api.listen(process.env.PORT | 8000)
