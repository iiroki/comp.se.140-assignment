const http = require('http')

const api = http.createServer((_req, res) => {
  res.writeHead(200)
  res.end()
})

module.exports = api
