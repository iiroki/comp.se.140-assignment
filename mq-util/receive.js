const amqp = require('amqplib/callback_api')

const EXCHANGE = 'default'

/**
 * Message queue: Receive messages from wanted topics.
 * @param {String} host Message queue host URL
 * @param {[String]} topics Topics to listen to
 * @param {(msg: amqp.Message) => void} consumer Message callback
 */
const receive = (host, topics, consumer) => {
  amqp.connect(`amqp://${host}`, (error0, connection) => {
    if (error0) throw error0

    connection.createChannel((error1, channel) => {
      if (error1) throw error1

      channel.assertExchange(EXCHANGE, 'topic', {
        durable: false
      })

      channel.assertQueue('', {
        exclusive: true
      }, (error2, q) => {
        if (error2) throw error2

        topics.forEach(key => {
          channel.bindQueue(q.queue, EXCHANGE, key)
        })

        console.log(`Waiting for messages from topics: ${topics}`)
        channel.consume(q.queue, msg => {
          console.log(`[${msg.fields.routingKey}] Received: ${msg.content.toString()}`)
          consumer(msg)
        }, {
          noAck: false
        })
      })
    })
  })
}

module.exports = receive
