const amqp = require('amqplib/callback_api')

const EXCHANGE = 'default'

/**
 * Message queue: Send message to certain topic.
 * @param {String} host Message queue host URL
 * @param {String} topic Topic where the message will be sent
 * @param {String} msg Message tcontent
 */
const send = (host, topic, msg) => {
  amqp.connect(`amqp://${host}`, (error0, connection) => {
    if (error0) throw error0

    connection.createChannel((error1, channel) => {
      if (error1) throw error1

      channel.assertExchange(EXCHANGE, 'topic', {
        durable: false
      })

      channel.publish(EXCHANGE, topic, Buffer.from(msg))
      console.log(`[${topic}] Sent: ${msg}`)
    })

    setTimeout(() => {
      connection.close()
    }, 500)
  })
}

module.exports = send
