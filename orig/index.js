const send = require('../mq-util/send')

const MQ_HOST = process.env.MQ_HOST
const SEND_TOPIC = 'my.o'
const MESSAGE_AMOUNT = 3
const BETWEEN_MESSAGES_MS = 3000
const STARTUP_DELAY_MS = 5000

/**
 * 1. Wait for 10 secs before sending messages
 * 2. Send messages to wanted topic
 * 3. Wait 3 secs between messages
 * 4. After sending all the messages, wait for 10 secs and start over.
 */
const main = async () => {
  await new Promise(resolve => setTimeout(resolve, STARTUP_DELAY_MS))

  for (let n = 1; n <= MESSAGE_AMOUNT; ++n) {
    send(MQ_HOST, SEND_TOPIC, `MSG_${n}`)
    await new Promise(resolve => setTimeout(resolve, BETWEEN_MESSAGES_MS))
  }
}

main()
