# COMP.SE.140: Assignment

## Message queue
My message queue implementation (exercise 6.1) was originally hosted on GitHub. I copied the implementation to
`message-queue` branch so it can be tested separately.

## Documentation
Documentation can be found TODO.

## Instructions TODO
Clone the repository and start the containers with the following commands:
```
$ git clone git@course-gitlab.tuni.fi:comp.se.140---fall-2021_2021-2022/kiviluoi.git
$ git checkout project
$ docker-compose build –-no-cache
$ docker-compose up -d
```
`Original` starts sending messages to the MQ after 5 seconds. Wait for at most 30 seconds, then run the following command:
```
$ curl localhost:8080
```
Output of the `curl` command should be like this:
```
YYYY-MM-DDThh:mm:ss.sssZ Topic my.o: MSG_1
YYYY-MM-DDThh:mm:ss.sssZ Topic my.i: Got MSG_1
YYYY-MM-DDThh:mm:ss.sssZ Topic my.o: MSG_2
YYYY-MM-DDThh:mm:ss.sssZ Topic my.i: Got MSG_2
YYYY-MM-DDThh:mm:ss.sssZ Topic my.o: MSG_3
YYYY-MM-DDThh:mm:ss.sssZ Topic my.i: Got MSG_3
```
Stop the containers with the following command:
```
$ docker-compose down
```
